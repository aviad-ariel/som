import numpy as np
import matplotlib.pyplot as plt


def draw(w1, w2, iterate, radius, l_rate, is_line=True):
  fig, ax = plt.subplots()
  if is_line:
    plt.plot(w1, w2, color='blue', marker='o', linestyle='dashed', 
             linewidth=2, markersize=12)
  else:
    plt.plot(w1, w2, color='blue', linestyle='None', marker='o', markersize=12)
  ax.set(xlabel='wieght 1', 
         ylabel='wieght 2',
         title='Kohonen algorithm (Iter: {}, Radius: {:.3f}, L_rate:{:.3f})'
         .format(iterate, radius, l_rate))
  ax.grid()
  plt.show()

def euclidean_dist(x, y, node_id):
  return np.sqrt((x[node_id] - x)**2 + (y[node_id] - y)**2)


def bmu(x ,y, node_id):
  ed = euclidean_dist(x, y, node_id)
  ed[node_id] = np.inf
  argmin_x = np.argmin(ed).item()
  return argmin_x


def choose_randomly(x):
  rnd = np.where(x == np.random.choice(x))
  return rnd if type(rnd) is not tuple else int(rnd[0][0])


def ord_vector(x, y, node_id):
  by_euclid_dist = -1 * euclidean_dist(x ,y, node_id)
  x_ord_idx = np.argsort(by_euclid_dist)
  y_ord_idx = np.argsort(by_euclid_dist)
  return x[x_ord_idx], y[y_ord_idx]


def update_neighbourhood(x, y, bmu_node, node_id, radius, alpha):
  ed = euclidean_dist(x, y, bmu_node)
  for idx, r in enumerate(ed):
    if -radius <= r <= radius:
      x[idx] += alpha * (x[node_id] - x[idx])
      y[idx] += alpha * (y[node_id] - y[idx])
  return x, y

def rand_non_uniform(x, y):
  x1 = np.random.uniform(-x,0,25)
  y1 = np.random.uniform(0,y,25)

  x2 = np.random.uniform(-x,0,25)
  y2 = np.random.uniform(-y,0,25)


  x = np.append(x1, x2)
  y = np.append(y1, y2)
  return x, y



def generate_circle_point(s_range, e_range, amount=100):
  ang = np.random.uniform(0, 2*np.pi, amount)
  r = np.random.uniform(s_range, e_range, amount)
  y = r * np.sin(ang)
  x = r * np.cos(ang)
  return x, y
